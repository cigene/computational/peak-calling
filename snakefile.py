import pandas as pd
import os.path

#Input 
REF = config["REF"] 
samples = pd.read_csv(config["SAMPLE_FILE"],sep="\t",dtype = str)
if ('path' not in list(samples.columns)) or ('sample' not in list(samples.columns)) or ('R1/R2' not in list(samples.columns)):
	sys.exit("Your sample file "+config["SAMPLE_FILE"]+" do not contain at least 3 columns path, sample, and R1/R2. Note that this file must be tab delimited.")
samples = samples.sort_values(by=['sample','path','R1/R2'])
if (('group' not in list(samples.columns)) or not(config["pooling"])):
    samples['group'] = samples['sample']
if ('control' in list(samples.columns)):
    CONTROL_SAMPLES = [S for S in samples['control'].unique().tolist() if str(S) != 'nan']
    idx = samples.index[samples['sample'].isin(CONTROL_SAMPLES)==False].tolist()
    ALL_GROUPS = samples['group'].loc[idx].unique().tolist()
else:
    CONTROL_SAMPLES = []
    ALL_GROUPS = samples['group'].unique().tolist()
ALL_SAMPLES = samples['sample'].unique().tolist()

#Define output dirs
OUT_DIR = config["OUT_DIR"]
FASTQC_DIR = OUT_DIR+"/"+config["FASTQC_SUBDIR"]
TRIMMED_DIR = OUT_DIR+"/"+config["TRIMMED_FASTQ_SUBDIR"]
ALIGNED_DIR = OUT_DIR+"/"+config["ALIGNMENT_SUBDIR"]
PEAK_DIR = OUT_DIR+"/"+config["PEAK_SUBDIR"]

#Define paths to program
FASTQC_PROGRAM = config["FASTQC_PROGRAM"]
TRIMGALORE_PROGRAM = config["TRIMGALORE_PROGRAM"]
NGMERGE_PROGRAM = config["NGMERGE_PROGRAM"]
GENRICH_PROGRAM = config["GENRICH_PROGRAM"]
BOWTIE2_PROGRAM =  config["BOWTIE2_PROGRAM"]
BWA_PROGRAM = config["BWA_PROGRAM"]

#Choices of program for cutadapter (TRIMGALORE or NGMERGE) and alignment (BOWTIE2 or BWA)
CUTADAPTER_PROGRAM = config["CUTADAPTER_PROGRAM"]
ALIGNMENT_PROGRAM = config["ALIGNMENT_PROGRAM"]
if (CUTADAPTER_PROGRAM != "NGMERGE" and CUTADAPTER_PROGRAM != "TRIMGALORE"):
    sys.exit("CUTADAPTER_PROGRAM in the config file should be either TRIMGALORE or NGMERGE")
if (ALIGNMENT_PROGRAM != "BWA" and ALIGNMENT_PROGRAM != "BOWTIE2"):
    sys.exit("ALIGNMENT_PROGRAM in the config file should be either BWA or BOWTIE2")
PEAK_CALLING_MODE = config["PEAK_CALLING_MODE"]
if (PEAK_CALLING_MODE != "ATAC" and PEAK_CALLING_MODE != "CHIP"):
    sys.exit("PEAK_CALLING_MODE in the config file should be either ATAC or CHIP")


#Data is paired end or single end
R12 = list(samples['R1/R2'].unique())
if (R12 != ["R1","R2"] and R12 != ["SE"]):
    sys.exit("Error in the tsv file: column R1/2 should contains value R1 or R2 for paired end data - and SE for single end data")

#Reference index path
if ALIGNMENT_PROGRAM=="BWA":
  if (os.path.isfile(REF+".amb") and os.path.isfile(REF+".ann") and os.path.isfile(REF+".bwt") and os.path.isfile(REF+".pac") and os.path.isfile(REF+".sa")):
    INDEX=REF+".amb"
  else:
    INDEX=OUT_DIR+"/"+config["ALIGNMENT_SUBDIR"]+"/ref/"+os.path.basename(REF)+".amb"
  REFBASE = os.path.splitext(INDEX)[0]
if ALIGNMENT_PROGRAM=="BOWTIE2":
  if (os.path.isfile(REF+".1.bt2")):
    INDEX=REF+".1.bt2"
  else:
    INDEX=OUT_DIR+"/"+config["ALIGNMENT_SUBDIR"]+"/ref/"+os.path.basename(REF)+".1.bt2"
  REFBASE = os.path.splitext(os.path.splitext(INDEX)[0])[0]
#

if len(R12)==2:
    PE = True
    FASTQC_DIR_PE = FASTQC_DIR
    FASTQC_DIR_SE = FASTQC_DIR+"tmp" #to avoid ambiguous outputs for paired end and single end cases; even though this folder will not be used in this case
    if (REFBASE == REF):
      ALIGNED_DIR_PE_GIVEN_INDEX = ALIGNED_DIR
      ALIGNED_DIR_SE_GIVEN_INDEX = ALIGNED_DIR+"tmp" 
      ALIGNED_DIR_PE_MY_INDEX = ALIGNED_DIR+"tmp"
      ALIGNED_DIR_SE_MY_INDEX = ALIGNED_DIR+"tmp"
    else:
      ALIGNED_DIR_PE_MY_INDEX = ALIGNED_DIR
      ALIGNED_DIR_SE_MY_INDEX = ALIGNED_DIR+"tmp"
      ALIGNED_DIR_PE_GIVEN_INDEX = ALIGNED_DIR+"tmp"
      ALIGNED_DIR_SE_GIVEN_INDEX = ALIGNED_DIR+"tmp"
else:
    PE = False
    CUTADAPTER_PROGRAM = "TRIMGALORE"
    FASTQC_DIR_SE = FASTQC_DIR
    FASTQC_DIR_PE = FASTQC_DIR+"tmp"
    if (REFBASE == REF):
      ALIGNED_DIR_SE_GIVEN_INDEX = ALIGNED_DIR
      ALIGNED_DIR_PE_GIVEN_INDEX = ALIGNED_DIR+"tmp" 
      ALIGNED_DIR_PE_MY_INDEX = ALIGNED_DIR+"tmp"
      ALIGNED_DIR_SE_MY_INDEX = ALIGNED_DIR+"tmp"
    else:
      ALIGNED_DIR_SE_MY_INDEX = ALIGNED_DIR
      ALIGNED_DIR_PE_MY_INDEX = ALIGNED_DIR+"tmp"
      ALIGNED_DIR_PE_GIVEN_INDEX = ALIGNED_DIR+"tmp"
      ALIGNED_DIR_SE_GIVEN_INDEX = ALIGNED_DIR+"tmp"

#Number of threads for each task
CUTADAPTER_THREADS = config["CUTADAPTER_THREADS"]
ALIGNMENT_THREADS = config["ALIGNMENT_THREADS"]

#Parameter for cutadapt
MIN_LENGTH = config["MIN_LENGTH"]
MIN_QUALITY = config["MIN_QUALITY"]
ADAPTER_SEQUENCE = config["ADAPTER_SEQUENCE"]
STRINGENCY = config["STRINGENCY"]
TRIM_N = config["TRIM_N"]
ERROR_RATE = config["ERROR_RATE"]

#Parameter for NGmerge
NGMERGE_E = config["NGMERGE_E"]

#Define final output files for the pipeline
if (config["z"]):
  OUT = [PEAK_DIR+"/"+str(F)+".narrowPeak.gz" for F in ALL_GROUPS] #peak files
else:
  OUT = [PEAK_DIR+"/"+str(F)+".narrowPeak" for F in ALL_GROUPS] #peak files
OUT = OUT+[ALIGNED_DIR+"/"+str(F)+".sorted.bam" for F in ALL_SAMPLES] #bam files
SE_OUT = OUT+[TRIMMED_DIR+"/"+str(F)+"_trimmed.fastq.gz" for F in ALL_SAMPLES]
PE_OUT = OUT+[TRIMMED_DIR+"/"+str(F)+"_trimmed_R1.fastq.gz" for F in ALL_SAMPLES]+[TRIMMED_DIR+"/"+str(F)+"_trimmed_R2.fastq.gz" for F in ALL_SAMPLES]
if (not(config["SKIP_FASTQC_RAW"])):
  SE_OUT = SE_OUT+[FASTQC_DIR+"/rawData/"+str(F)+"_fastqc.html" for F in ALL_SAMPLES] #fastqc report for raw data
  PE_OUT = PE_OUT+[FASTQC_DIR+"/rawData/"+str(F)+"_R1_fastqc.html" for F in ALL_SAMPLES]+[FASTQC_DIR+"/rawData/"+str(F)+"_R2_fastqc.html" for F in ALL_SAMPLES] #fastqc report for raw data
  TRIMMED_DIR0 = TRIMMED_DIR+"tmp"
  TRIMMED_DIR1 = TRIMMED_DIR
else:
  TRIMMED_DIR0 = TRIMMED_DIR
  TRIMMED_DIR1 = TRIMMED_DIR+"tmp"
if (not(config["SKIP_FASTQC_TRIMMED"])):
  SE_OUT = SE_OUT+[FASTQC_DIR+"/trimmedData/"+str(F)+"_trimmed_fastqc.html" for F in ALL_SAMPLES] #fastqc report for trimmed data
  PE_OUT = PE_OUT+[FASTQC_DIR+"/trimmedData/"+str(F)+"_trimmed_R1_fastqc.html" for F in ALL_SAMPLES]+[FASTQC_DIR+"/trimmedData/"+str(F)+"_trimmed_R2_fastqc.html" for F in ALL_SAMPLES] #fastqc report for trimmed data

SKIP_TRIM = config['SKIP_TRIM']

rule all:
    input:
        OUT_DIR+"/metaData.txt",
        PE_OUT if PE else SE_OUT

rule printMetaInfo:
  params:
    config = config
  output:
    OUT_DIR+"/metaData.txt"
  script:
    "scripts/printMetaDataInfo.R"

def get_fq1(wildcards):
    idx = samples.index[(samples['sample'] == wildcards.sample) & (samples['R1/R2']=="R1")].tolist()
    return(samples['path'].loc[idx].tolist())

def get_fq2(wildcards):
    idx = samples.index[(samples['sample'] == wildcards.sample) & (samples['R1/R2']=="R2")].tolist()
    return(samples['path'].loc[idx].tolist())

def get_fq(wildcards):
    idx = samples.index[(samples['sample'] == wildcards.sample) & (samples['R1/R2']=="SE")].tolist()
    return(samples['path'].loc[idx].tolist())


rule trimadapter_pe:
    input:
        R1 = get_fq1,
        R2 = get_fq2
    output:
        R1 = TRIMMED_DIR0+"/{sample}_trimmed_R1.fastq.gz",
        R2 = TRIMMED_DIR0+"/{sample}_trimmed_R2.fastq.gz"
    params:
        E = NGMERGE_E,
        Q = MIN_QUALITY,
        L = MIN_LENGTH, 
        A = ADAPTER_SEQUENCE,
        S = STRINGENCY,
        N = TRIM_N,
        ER = ERROR_RATE
    threads:
        CUTADAPTER_THREADS
    shell:
        """
        cat {input.R1} > {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz  #merge lanes if necessary
        cat {input.R2} > {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz  #merge lanes if necessary
	if [ {SKIP_TRIM} == "True" ]; then
	  mv {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {output.R1};
	  mv {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz {output.R2};	
	else #cut adapter
          if [ {CUTADAPTER_PROGRAM} == "NGMERGE" ]; then
            {NGMERGE_PROGRAM} -n {threads} -e {params.E} -z -a -1 -1 {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz -2 {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}/{wildcards.sample}
            mv {TRIMMED_DIR}/{wildcards.sample}_1.fastq.gz {output.R1}
            mv {TRIMMED_DIR}/{wildcards.sample}_2.fastq.gz {output.R2}
          else
            if [ {params.N} == "True" ]; then
              if [ {params.A} == "NA" ]; then
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} -e {params.ER} --paired {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}
              else  
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} --adapter {params.A} -e {params.ER} --paired {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}
              fi  
            else  
              if [ {params.A} == "NA" ]; then
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} -e {params.ER} --paired {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}
              else
              fi  
            fi
            mv {TRIMMED_DIR}/{wildcards.sample}_R1_val_1.fq.gz {output.R1}
            mv {TRIMMED_DIR}/{wildcards.sample}_R2_val_2.fq.gz {output.R2}
	  fi
          rm -f {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz
          rm -f {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz
	fi
        """

rule fastqc_trimadapter_pe:
    input:
        R1 = get_fq1,
        R2 = get_fq2
    output:
        FASTQC_R1 = FASTQC_DIR_PE+"/rawData/{sample}_R1_fastqc.html",
        FASTQC_R2 = FASTQC_DIR_PE+"/rawData/{sample}_R2_fastqc.html",
        R1 = TRIMMED_DIR1+"/{sample}_trimmed_R1.fastq.gz",
        R2 = TRIMMED_DIR1+"/{sample}_trimmed_R2.fastq.gz"
    params:
        E = NGMERGE_E,
        Q = MIN_QUALITY,
        L = MIN_LENGTH,
        A = ADAPTER_SEQUENCE,
        S = STRINGENCY,
        N = TRIM_N,
        ER = ERROR_RATE
    threads:
        CUTADAPTER_THREADS
    shell:
        """
        cat {input.R1} > {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz  #merge lanes if necessary
        cat {input.R2} > {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz  #merge lanes if necessary
        {FASTQC_PROGRAM} -t 2 -o {FASTQC_DIR}/rawData {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz
	if [ {SKIP_TRIM} == "True" ]; then
          mv {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {output.R1};
          mv {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz {output.R2};  
        else  #cut adapter
          if [ {CUTADAPTER_PROGRAM} == "NGMERGE" ]; then
            {NGMERGE_PROGRAM} -n {threads} -e {params.E} -z -a -1 -1 {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz -2 {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}/{wildcards.sample}
            mv {TRIMMED_DIR}/{wildcards.sample}_1.fastq.gz {output.R1}
            mv {TRIMMED_DIR}/{wildcards.sample}_2.fastq.gz {output.R2}
          else
            if [ {params.N} == "True" ]; then
              if [ {params.A} == "NA" ]; then
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} -e {params.ER} --paired {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}
              else  
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} --adapter {params.A} -e {params.ER} --paired {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}
              fi  
            else  
              if [ {params.A} == "NA" ]; then
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} -e {params.ER} --paired {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}
              else
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} --adapter {params.A} -e {params.ER} --paired {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz -o {TRIMMED_DIR}
              fi  
            fi
            mv {TRIMMED_DIR}/{wildcards.sample}_R1_val_1.fq.gz {output.R1}
            mv {TRIMMED_DIR}/{wildcards.sample}_R2_val_2.fq.gz {output.R2}
          fi
          rm -f {TRIMMED_DIR}/{wildcards.sample}_R1.fastq.gz
          rm -f {TRIMMED_DIR}/{wildcards.sample}_R2.fastq.gz
        fi
        """

rule fastqc_trimmed_pe:
  input:
    R1 = TRIMMED_DIR+"/{sample}_trimmed_R1.fastq.gz",
    R2 = TRIMMED_DIR+"/{sample}_trimmed_R2.fastq.gz"
  output:
    FASTQC_TRIMMED_R1 = FASTQC_DIR_PE+"/trimmedData/{sample}_trimmed_R1_fastqc.html",
    FASTQC_TRIMMED_R2 = FASTQC_DIR_PE+"/trimmedData/{sample}_trimmed_R2_fastqc.html"
  shell:
    """
    {FASTQC_PROGRAM} -o {FASTQC_DIR}/trimmedData {input.R1} {input.R2}
    """
 
rule trimadapter_se:
    input:
        get_fq
    output:
        TRIMMED_DIR0+"/{sample}_trimmed.fastq.gz"
    params:
        Q = MIN_QUALITY,
        L = MIN_LENGTH,
        A = ADAPTER_SEQUENCE,
        S = STRINGENCY,
        N = TRIM_N,
        ER = ERROR_RATE
    shell:
        """
        cat {input} > {TRIMMED_DIR}/{wildcards.sample}.fastq.gz  #merge lanes if necessary
        if [ {params.N} == "True" ]; then
              if [ {params.A} == "NA" ]; then
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} -e {params.ER} {TRIMMED_DIR}/{wildcards.sample}.fastq.gz -o {TRIMMED_DIR}
              else  
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} --adapter {params.A} -e {params.ER} {TRIMMED_DIR}/{wildcards.sample}.fastq.gz -o {TRIMMED_DIR}
              fi  
        else  
              if [ {params.A} == "NA" ]; then
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} -e {params.ER} {TRIMMED_DIR}/{wildcards.sample}.fastq.gz -o {TRIMMED_DIR}
              else
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} --adapter {params.A} -e {params.ER} {TRIMMED_DIR}/{wildcards.sample}.fastq.gz -o {TRIMMED_DIR}
              fi  
        fi
        mv {TRIMMED_DIR}/{wildcards.sample}_trimmed.fq.gz {TRIMMED_DIR}/{wildcards.sample}_trimmed.fastq.gz 
        rm -f {TRIMMED_DIR}/{wildcards.sample}.fastq.gz
        """
        
rule fastqc_trimadapter_se:
    input:
        get_fq
    output:
        FASTQC_DIR_SE+"/rawData/{sample}_fastqc.html",
        TRIMMED_DIR1+"/{sample}_trimmed.fastq.gz"
    params:
        Q = MIN_QUALITY,
        L = MIN_LENGTH,
        A = ADAPTER_SEQUENCE,
        S = STRINGENCY,
        N = TRIM_N,
        ER = ERROR_RATE
    shell:
        """
        cat {input} > {TRIMMED_DIR}/{wildcards.sample}.fastq.gz  #merge lanes if necessary
        {FASTQC_PROGRAM} -o {FASTQC_DIR}/rawData {TRIMMED_DIR}/{wildcards.sample}.fastq.gz
        if [ {params.N} == "True" ]; then
              if [ {params.A} == "NA" ]; then
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} -e {params.ER} {TRIMMED_DIR}/{wildcards.sample}.fastq.gz -o {TRIMMED_DIR}
              else  
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} --adapter {params.A} -e {params.ER} {TRIMMED_DIR}/{wildcards.sample}.fastq.gz -o {TRIMMED_DIR}
              fi  
        else  
              if [ {params.A} == "NA" ]; then
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} -e {params.ER} {TRIMMED_DIR}/{wildcards.sample}.fastq.gz -o {TRIMMED_DIR}
              else
                {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} --adapter {params.A} -e {params.ER} {TRIMMED_DIR}/{wildcards.sample}.fastq.gz -o {TRIMMED_DIR}
              fi  
        fi
        mv {TRIMMED_DIR}/{wildcards.sample}_trimmed.fq.gz {TRIMMED_DIR}/{wildcards.sample}_trimmed.fastq.gz 
        rm -f {TRIMMED_DIR}/{wildcards.sample}.fastq.gz
        """

rule fastqc_trimmed_se:
  input:
    R = TRIMMED_DIR+"/{sample}_trimmed.fastq.gz"
  output:
    FASTQC_DIR_SE+"/trimmedData/{sample}_trimmed_fastqc.html"
  shell:
    """
    {FASTQC_PROGRAM} -o {FASTQC_DIR}/trimmedData {input.R}
    """
    
rule build_index:
  input:
    REF
  output:
    INDEX
  shell:
    """
      if [ {ALIGNMENT_PROGRAM} == "BOWTIE2" ]; then
          {BOWTIE2_PROGRAM}-build {REF} {REFBASE}
      fi
      if [ {ALIGNMENT_PROGRAM} == "BWA" ]; then
          {BWA_PROGRAM} index -p {REFBASE} {REF}
      fi
    """
      
rule alignment_pe_given_index:
    input:
        R1 = OUT_DIR+"/"+config["TRIMMED_FASTQ_SUBDIR"]+"/{sample}_trimmed_R1.fastq.gz",
        R2 = OUT_DIR+"/"+config["TRIMMED_FASTQ_SUBDIR"]+"/{sample}_trimmed_R2.fastq.gz",
        REF = INDEX
    output:
        BAM = ALIGNED_DIR_PE_GIVEN_INDEX+"/{sample}.sorted.bam",
        LOG = ALIGNED_DIR_PE_GIVEN_INDEX+"/logs/{sample}.log",
        STATS = ALIGNED_DIR_PE_GIVEN_INDEX+"/stats/{sample}.stats"
    threads:
        ALIGNMENT_THREADS
    shell:
      """
        if [ {ALIGNMENT_PROGRAM} == "BOWTIE2" ]; then
            {BOWTIE2_PROGRAM} --very-sensitive -p {threads} -k 10 -x {REFBASE} -1 {input.R1} -2 {input.R2} 2> {output.LOG} | samtools view -u - | samtools sort -n -o {output.BAM}
        fi
        if [ {ALIGNMENT_PROGRAM} == "BWA" ]; then
            {BWA_PROGRAM} mem -t {threads} {REFBASE} {input.R1} {input.R2} 2> {output.LOG} | samtools view -u - | samtools sort -n -o {output.BAM}
        fi
        samtools stats {output.BAM} > {output.STATS}
      """

rule alignment_pe_my_index:
    input:
        R1 = OUT_DIR+"/"+config["TRIMMED_FASTQ_SUBDIR"]+"/{sample}_trimmed_R1.fastq.gz",
        R2 = OUT_DIR+"/"+config["TRIMMED_FASTQ_SUBDIR"]+"/{sample}_trimmed_R2.fastq.gz",
        REF = INDEX
    output:
        BAM = ALIGNED_DIR_PE_MY_INDEX+"/{sample}.sorted.bam",
        LOG = ALIGNED_DIR_PE_MY_INDEX+"/logs/{sample}.log",
        STATS = ALIGNED_DIR_PE_MY_INDEX+"/stats/{sample}.stats"
    threads:
        ALIGNMENT_THREADS
    shell:
      """
        if [ {ALIGNMENT_PROGRAM} == "BOWTIE2" ]; then
            {BOWTIE2_PROGRAM} --very-sensitive -p {threads} -k 10 -x {REFBASE} -1 {input.R1} -2 {input.R2} 2> {output.LOG} | samtools view -u - | samtools sort -n -o {output.BAM}
        fi
        if [ {ALIGNMENT_PROGRAM} == "BWA" ]; then
            {BWA_PROGRAM} mem -t {threads} {REFBASE} {input.R1} {input.R2} 2> {output.LOG} | samtools view -u - | samtools sort -n -o {output.BAM}
        fi
        samtools stats {output.BAM} > {output.STATS}
      """
        
rule alignment_se_given_index:
    input:
        R =  OUT_DIR+"/"+config["TRIMMED_FASTQ_SUBDIR"]+"/{sample}_trimmed.fastq.gz",
        REF = INDEX
    output:
        BAM = ALIGNED_DIR_SE_GIVEN_INDEX+"/{sample}.sorted.bam",
        LOG = ALIGNED_DIR_SE_GIVEN_INDEX+"/logs/{sample}.log",
        STATS = ALIGNED_DIR_SE_GIVEN_INDEX+"/stats/{sample}.stats"
    threads:
        ALIGNMENT_THREADS
    shell:
       """
        if [ {ALIGNMENT_PROGRAM} == "BOWTIE2" ]; then
            {BOWTIE2_PROGRAM} --very-sensitive -p {threads} -k 10 -x {REFBASE} -U {input.R} 2> {output.LOG} | samtools view -u - | samtools sort -n -o {output.BAM}
        fi
        if [ {ALIGNMENT_PROGRAM} == "BWA" ]; then
            {BWA_PROGRAM} mem -t {threads} {REFBASE} {input} 2> {output.LOG} | samtools view -u - | samtools sort -n -o {output.BAM}
        fi
        samtools stats {output.BAM} > {output.STATS}
        """


rule alignment_se_my_index:
    input:
        R =  OUT_DIR+"/"+config["TRIMMED_FASTQ_SUBDIR"]+"/{sample}_trimmed.fastq.gz",
        REF = INDEX
    output:
        BAM = ALIGNED_DIR_SE_MY_INDEX+"/{sample}.sorted.bam",
        LOG = ALIGNED_DIR_SE_MY_INDEX+"/logs/{sample}.log",
        STATS = ALIGNED_DIR_SE_MY_INDEX+"/stats/{sample}.stats"
    threads:
        ALIGNMENT_THREADS
    shell:
       """
        if [ {ALIGNMENT_PROGRAM} == "BOWTIE2" ]; then
            {BOWTIE2_PROGRAM} --very-sensitive -p {threads} -k 10 -x {REFBASE} -U {input} 2> {output.LOG} | samtools view -u - | samtools sort -n -o {output.BAM}
        fi
        if [ {ALIGNMENT_PROGRAM} == "BWA" ]; then
            {BWA_PROGRAM} mem -t {threads} {REFBASE} {input} 2> {output.LOG} | samtools view -u - | samtools sort -n -o {output.BAM}
        fi
        samtools stats {output.BAM} > {output.STATS}
        """

def get_group(wildcards):
    idx = samples.index[(samples['group'] == wildcards.group)].tolist()
    samples_of_group = samples['sample'].loc[idx].unique().tolist()
    if (len(CONTROL_SAMPLES)==0):
        controls_of_group = []
    else:
        controls_of_group = [C for C in samples['control'].loc[idx].unique().tolist() if str(C) != 'nan']
    return([ALIGNED_DIR+"/"+s+".sorted.bam" for s in samples_of_group+controls_of_group])

rule peak_calling:
    input:
        get_group
    output:
        NP = (PEAK_DIR+"/{group}.narrowPeak") if (not(config['z'])) else (PEAK_DIR+"/{group}.narrowPeak.gz")
    params:
        MODE = PEAK_CALLING_MODE
    run:
        bams=[S for S in input if os.path.basename(re.sub(".sorted.bam$","",S)) not in CONTROL_SAMPLES]
        list_bam = " -t "+','.join(map(str,bams))
        if (len(CONTROL_SAMPLES)==0):
            list_control = ""
        else:
            list_control = []
            for S in bams:
              idx=samples.index[(samples['sample']==os.path.basename(re.sub(".sorted.bam$","",S)))].tolist()
              control=samples['control'].loc[idx]
              if control.isnull().values.any():
                control="null"
              else:
                control=ALIGNED_DIR+"/"+control.unique().tolist()[0]+".sorted.bam"
              list_control.append(control)
            list_control = " -c "+','.join(map(str,list_control))
        out_command = " -o "+PEAK_DIR+"/{wildcards.group}.narrowPeak"
        if (config["f"]):
            out_command = out_command+" -f "+PEAK_DIR+"/{wildcards.group}.f.bedGraph"
            if (config["z"]):
              out_command = out_command+".gz"
        if (config["k"]):
            out_command = out_command+" -k "+PEAK_DIR+"/{wildcards.group}.k.bedGraph"
        if (config["b"]):
           out_command = out_command+" -b "+PEAK_DIR+"/{wildcards.group}.bed"
        if (config["R"]):
           out_command = out_command+" -R "+PEAK_DIR+"/{wildcards.group}.PCR"   
        if (config["z"]):
          out_command = out_command+" -z"
        if (config["P"]):
          out_command = out_command+" -P "
          if (not(config["f"])):  
            out_command = out_command+" -f "+PEAK_DIR+"/{wildcards.group}.f.bedGraph"
            if (config["z"]):
              out_command = out_command+".gz"

        exclude = " -m "+str(config["m"])
        if (config["e"] != "NA"):
            exclude = " -e "+config["e"]
        if (config["E"] != "NA"): 
            exclude = exclude+" -E "+config["E"]
        if (config["y"] or PE == False):
            exclude = exclude+" -y"
        
        if (config["useQ"]):
          options = " -q "+str(config["q"])
        else:
          options = " -p "+str(config["p"])
        options = options+" -a "+str(config["a"])+" -l "+str(config["l"])+" -g "+str(config["g"])  
        if (params.MODE == "ATAC"):
            options = options+" -j -d "+str(config["d"])
            if (config["D"] == "TRUE"):
              options = options+" -D" 
        if (config["r"]):
          options = options+" -r"
        if (config["X"]):
          options = options+" -X"
            
        command = GENRICH_PROGRAM+" -v "+list_bam+list_control+out_command+exclude+options
        print(command)
        shell(command)
