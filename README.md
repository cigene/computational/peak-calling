The pipeline aims to analyse ATAC-seq or ChIP-seq data, and includes the following steps:

- run fastqc
- cut adapter using either Trim_galore or NGmerge and run fastqc again on the trimmed data
- align trimmed data to the reference genome using either bowtie2 or bwa mem
- peak calling using Genrich under either ATAC-seq mod or ChIP-seq mod

## Software requirements:
Note that the path to each software is editable in the config file (see below)

- Snakemake version 5.5.0 : https://snakemake.readthedocs.io/en/stable/getting_started/installation.html
- fastqc : https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
- Trim_galore : https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/
- NGmerge (optional) : https://github.com/jsh58/NGmerge
- bowtie : http://bowtie-bio.sourceforge.net/bowtie2/
- bwa : http://bio-bwa.sourceforge.net
- Genrich : https://github.com/jsh58/Genrich

## Usage

### Edit a sample tsv file to specify information about the input raw fastq files:
This should be a __tab seperated__ file that contains the following columns:

- path : the *absolute path* to each fastq file
- sample : the sample name of each file
- R1/R2	: should be either "R1" or "R2" or "SE" depending whether the file is first end, sencond end, or single end data. Note that your samples must be all paired-end, or all single end. A mixed data set of paired-end and single end files will not work.
- group (optional): if the data contains replicated samples, then define the group name here. When calling peak, Genrich will calculate the p-value of each individual replicate and then combine them to generate peaks for the whole group.
- control (optional): if you have control samples, then this column is used to specify the control sample name for each experimental sample. Put 'null' for a sample that doesn't have control sample. The control sample itself can also have value 'null' at this column.
    
For examples of tsv file, see the files 'data.tsv', 'data_group.tsv', or 'data_control.tsv'

### Edit a config file based on the template *config.yaml* to specify:

- Your sample tsv file name that you've editted above in the SAMPLE_FILE field. Alternatively this can also be specified in the command line when running the pipeline (see below).
- Select the program to cut the adapters in CUTADAPTER_PROGRAM field (either "TRIM_GALORE" or "NGMERGE"), the alignment program in ALIGNMENT_PROGRAM field (either "BOWTIE2" or "BWA"), and the peak calling mode (either "ATAC" or "CHIP")
- The path to the index of the reference genome for alignment (either bowtie2 index or bwa index). Alternatively, users can provide the reference sequence in fasta file so that the pipeline will build the index following the selected aligner program.
- The path to each software should also be adjusted if necessary.
- Other parameters for each software, the number of threads for each task.

### Run the pipeline: 

After having your tsv and suppose that your config file is myConfig.yaml, you can run the pipeline with the command (for example using 8 cores):

  **snakemake --cores 8 -s snakefile.py --configfile myConfig.yaml**     
  
Alternatively, the users can overwirte the options in the config file when running snakemake in the command line, for example:

  **snakemake --cores 8 -s snakefile.py --configfile myConfig.yaml --config SAMPLE_FILE=data.tsv PEAK_CALLING_MODE=ATAC OUT_DIR=test**     
  
This command is to included in your submitting script when you want to run the pipeline on a cluster. Remember to load the necessary softwares and allocate enough memory, cores.

## Output structure

The pipeline generates inside the output folder (*OUT_DIR* in the config file) 4 sub-folders with the default names (could be changed by editting the config file):

- __fastqc__: contain the fastqc reports of the raw and trimmed data in the 2 sub-folders *rawData* and *trimmedData*. In *rawData*, you can find the fastqc report of each file under the name {sample}_R1_fastqc.\* and {sample}_R2_fastqc.\*. In *trimmedData*, you can find the fastqc report of each file under the name {sample}_trimmed_R1_fastqc.\* and {sample}_trimmed_R2_fastqc.\*
- __trimmed_fastq__: contain the fastq files after trimming adapters under the name {sample}_trimmed_R1.fastq.gz and {sample}_trimmed_R2.fastq.gz
- __alignments__: contain the bam files (sorted by query name) when aligning the trimmed fastq files to the reference genome under the name {sample}.sorted.bam. This folder also has 2 sub-folders *logs* (the log file of alignment process) and *stats* (result files of *samtools stats* on the alignment files)
- __peaks__: contain the results of peak calling including narrowPeak files {group}.narrowPeak.gz. Depending on your selections, it could also contain the bedGraph-ish files for p/q values {group}.f.bedGraph/{group}.k.bedGraph, and PCR duplicates file {group}.PCR


