#!/bin/bash
#SBATCH --nodes=1  
#SBATCH --partition=smallmem

module load anaconda3
module load genrich/0.5
module load bwa/0.7.17

snakemake --cores 20 -s snakefile.py --configfile config.yaml --config SAMPLE_FILE=data.tsv 